## Leitfragen und Übungsaufgaben zum orangen Grad des Clean-Code-Developers

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111334923701270625</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/cleancode-oranger-grad-uebung</span>

> **tl/dr;** _(ca. 30 min Bearbeitungszeit): Leitfragen und Übungsaufgaben zu CleanCode: Der Artikel enthält eine Reihe von Leitfragen zum Verständnis der Prinzipien (SLA, SRP, SoC, Source Code Conventions) und Praktiken (Issue Tracking, Integrationstest, Lesen, Reviews), die im orangenen Grad des Clean-Code-Developers zusammengefasst sind._

Um die Leitfragen beantworten zu können, solltest Du Dich zunächst mit dem [orangenen Grad des Clean Code Developers](https://clean-code-developer.de/die-grade/orangener-grad/) beschäftigen - oder auf anderem Weg mit den Prinzipien (SLA, SRP, SoC, Source Code Conventions) und den Praktiken (Issue Tracking, Integrationstest, Lesen, Reviews) vertraut machen.

![Dall-E hat einen Programmierer mit orangenem Gürtel vor'm PC gezeichnet (nach Gustav Klimmt)](images/DALL·E%202023-11-06%2012.50.31%20-%20Ein%20Programmierer%20sitzt%20vor%20einem%20PC%20und%20trägt%20einen%20Karateanzug%20mit%20orangenem%20Gürtel.%20Das%20Bild%20ist%20von%20Gustav%20Klimmt%20gemalt-small.webp)

### Leitfragen zum orangen Grad

1. Was wird unter _Abstraction_ im Sinne des _Single Layer of Abstraction_-Prinzips (SLA) verstanden? <button onclick="toggleAnswer('ccog1')">Antwort</button>

    <span class="hidden-answer" id="ccog1">

    Abstrahieren beschreibt in der Informatik die Vereinfachung von komplexen Zusammenhängen, eine Reduktion auf die wesentlichen Eigenschaften. 
    
    In diesem Fall ist nicht die Abstraktion im Sinne der Objektorientierten Programmierung (OOP) gemeint (nicht instanziierbare Interfaces und abstrakte Klassen statt konkreter Klassen), sondern die Ebene (oder der Detaillierungsgrad), auf der das Problem im jeweiligen Modul gelöst wird. 
    
    Programmkomponenten, die eine grobe Einteilung des Programms in Schichten umsetzen, befinden sich auf einer komplett anderen Abstraktionsebene als Programmteile, die einzelne Bits in Datenbanken setzen. In der großen Übersicht befinden wir uns auf einem sehr hohen Abstraktionsniveau (es wird viel vereinfacht), in kleinen Programmbestandteilen sind die Zusammenhänge viel näher an dem jeweiligen Ausgangsproblem und es wird deutlich weniger vereinfacht.

    Vergleichsweise einfache Operationen, die ggf. direkt auf der Hardware laufen stellen in der Softwareprogrammierung das niedrigste Abstraktionsniveau dar. Die Schichten, die darauf aufbauen, um komplexere Aufgaben zu lösen, stehen somit auf höheren Abstraktionsebenen. 

    </span>


1. Welche Folgen hat das _Single Layer of Abstraction_-Prinzip für die Gliederung / den Aufbau von Klassen? <button onclick="toggleAnswer('ccog2')">Antwort</button>

    <span class="hidden-answer" id="ccog2">

    Öffentliche Methoden hohen Abstraktionsniveaus sollten im Quelltest oberhalb stehen. Die von ihnen direkt aufgerufenen Methoden (niedrigeren Abstraktionsniveaus) sollten dann folgen uns so weiter. 
    
    So ist eine Klasse von oben nach unten lesbar und jede/r Leser/in kann selbst entscheiden, bis zu welchem Detailgrad (hohes Detailgrad = niedrige Abstraktion) gelesen werden soll.

    </span>

1. Nenne Beispiele für unterschiedliche Abstraktionslevel im Sinne des _Single Layer of Abstraction_-Prinzips. <button onclick="toggleAnswer('ccog3')">Antwort</button>

    <span class="hidden-answer" id="ccog3">

    Methodenaufruf des Frameworks, Methodenaufruf der JavaAPI, Methodenaufruf der Anwendung.

    Sehr niedriges Abstraktionsniveau: Methode, die nur Grundfunktionen der Programmiersprache nutzt.
            
    Sehr hohes Abstraktionsniveau: Methode, die auf spezialisierte Klassen eines Frameworks zugreift (und somit auf viele Abstraktionsschichten aufbaut).

    </span>

1. Was versteht man unter einer "Verantwortlichkeit" (_Responsibility_) im Sinne des _Single Responsibility Principles_ (SRP)? <button onclick="toggleAnswer('ccog4')">Antwort</button>

    <span class="hidden-answer" id="ccog4">

    Verantwortlichkeiten sind die Ziele und Ergebnisse einer Klasse/Methode. Die Verantwortlichkeiten unterschiedlicher Klassen/Methoden können sehr ähnlich sein (z.B. Berechnung von Steuern, Berechnung von Abgaben).
    
    Programmteile (Klassen, Methoden) haben die Verantwortlichkeit, kleine, isolierte und konkrete Teilprobleme des Softwaresystems zu lösen.
    
    </span>

1. Was versteht man unter einem "Belang" (_Concern_) im Sinne des _SoC_? <button onclick="toggleAnswer('ccog7')">Antwort</button>

    <span class="hidden-answer" id="ccog7">

    Concerns sind Übermengen von Responsibilities: beispielsweise wird der Concern einer Hauptfunktionalität eines Programms (Anforderung) durch mehrere Klassen und Methoden umgesetzt.

    Sie stehen für komplett verschiedene Zwecke, sind also orthogonal zueinander (überschneidungsfrei in den Funktionalitäten).
        
    Innerhalb einer Codeeinheit sollten Concerns getrennt werden (in Codeblöcke (bzw. Exception, Logging), Methoden, Klassen).

    </span>

1. Wie unterscheidet sich eine Verantwortlichkeit (im Sinne des _Single Responsibility Principles_) von einem Belang (im Sinne des _Separation of Concerns_)? <button onclick="toggleAnswer('ccog5')">Antwort</button>

    <span class="hidden-answer" id="ccog5">

    Belange haben eine übergeordnete Funktion, sie beschreiben abstrakte, allgemeine Aufgaben. Diese abstrakten Aufgaben werden an unterschiedlichen Stellen durch mehrere Klassen/Methoden umgesetzt.

    Im Gegensatz zu Verantwortlichkeiten (_Responsibilities_) gibt es im Idealfall keine Funktionsüberschneidungen bei mehreren Belangen (sie sind orthogonal).

    Verantwortlichkeiten (_Responsibilities_) lösen kleinere isolierte Probleme und werden auf Ebene einer Klasse oder Methode realisiert.

    Ein Belang kann durch mehrere Verantwortlichkeiten in unterschiedlichen Klassen umgesetzt werden.

    Beispiel: Die Datenpersistenz (Speicherung von Daten, als abstrakte Idee) ist ein Belang. Sie wird durch viele Klassen und Frameworks umgesetzt. Das Speichern eine Adresse (konkrete Klasse) ist aber die Verantwortlichkeit einer einzelnen Klasse (z.B. des AdressModell).

    </span>

1. Nenne Beispiele für Verantwortlichkeiten und Belange eines aktuellen Projekts, an dem Du arbeitest! <button onclick="toggleAnswer('ccog6')">Antwort</button>

    <span class="hidden-answer" id="ccog6">

    - Belang (_Concern_): z.B. Datenpersistenz, Protokollierung, Exception-Handling, Darstellung, Caching, Busineslogic, Transaktionen

    - Verantwortlichkeit (_Responsibility_): Speicherung einer spezifischen Entität, Berechnung eines spezifischen Steuersatzes, Validierung eines spezifischen Werts.
    
    </span>
            
1. In welchen Kontexten sind _Coding Conventions_ wie z.B. Namensregeln, die Attribute von lokalen Variablen unterscheidet, wichtiger, in welchen Kontexten weniger wichtig? <button onclick="toggleAnswer('ccog8')">Antwort</button>

    <span class="hidden-answer" id="ccog8">

    Coding Conventions sind insbesondere in großen Projekten mit vielen Mitwirkenden, in großen Klassen oder bei fehlender Unterstützung durch die IDE wichtig.

    Sie müssen v.a. auch eingehalten werden, wenn im Rahmen des Software-Deployments auch über _Linter_ die Einhaltung bestimmter Conventionen geprüft wird (z.B. bei Python über PEP8).

    </span>

1. Nenne Beispiele für Namensregeln (_Coding Conventions_) für Attribute und lokale Variablen! <button onclick="toggleAnswer('ccog9')">Antwort</button>

    <span class="hidden-answer" id="ccog9">

    Attribut: `this.name`

    lokale Variable: `_name`, `myName`

    </span>

1. Welche weiteren Namensregeln oder Coding Conventions kennst Du? <button onclick="toggleAnswer('ccog10')">Antwort</button>

    <span class="hidden-answer" id="ccog10">

    z.B. Java/C++: Klassen groß, Rest klein; Konstanten GROSS
        
    Python: siehe PEP8, z.B. : zwei Leerzeilen vor neuer Funktion, Dunder (doppelter Unterstrich) für private Attribute...

    </span>


1. Als Beispiel für _CodingConventions_ sei die mittlerweile veraltete _ungarische Notation_ genannt: Hierbei geben vorangestellte Prefixe Auskunft über den Datentyp einer Variablen (z.B. `strName`, `intAnzahl`, `txtEingabefeld`, `iZahl`, `checkboxIstWahr`). Welche Vorteile / Nachteile bietet die Code-Convention, Variablen mit _ungarischer Notation_ zu nutzen? <button onclick="toggleAnswer('ccog11')">Antwort</button>

    <span class="hidden-answer" id="ccog11">

    Nachteile: Probleme bei sich ändernden Datentypen, unnötig bei IDE mit Typisierungsunterstützung, Redundante Information (Datentyp in typsicheren Sprachen in Deklaration bereits genannt).
    
    Vorteile: Nach wie vor üblich in einigen Programmierumgebungen, ggf. hilfreich bei fehlender IDE-Unterstützung für Datentypen.

    (Ungarische Notation ist eine veraltete Coding Convention, an der man gut erkennen kann, dass Konventionen sich über die Zeit ändern können.)

    </span>

1. Warum kann das Refaktorisierungsmuster _Extract Method_ helfen, den Code von (unnötigen) Kommentaren zu befreien? <button onclick="toggleAnswer('ccog12')">Antwort</button>

    <span class="hidden-answer" id="ccog12">

    Werden Codezeilen in Methode ausgegliedert sollte der Methodenname erklären, was die Coderzeilen tun: ein Kommentar ist dadurch unnötig)

    </span>

    ![Dall-E hat einen Programmierer mit orangenem Gürtel vor'm PC gezeichnet (nach Gustav Klimmt)](images/DALL·E%202023-11-06%2012.50.36%20-%20Ein%20Programmierer%20sitzt%20vor%20einem%20PC%20und%20trägt%20einen%20Karateanzug%20mit%20orangenem%20Gürtel.%20Das%20Bild%20ist%20von%20Gustav%20Klimmt%20gemal-small.webp)

1. Was sollte in Kommentaren genannt werden, und was nicht? <button onclick="toggleAnswer('ccog13')">Antwort</button>

    <span class="hidden-answer" id="ccog13">

    Kommentiert werden sollte nicht **_was_** man tut, sondern **_wieso_** man etwas tut und auch **_wieso_** man etwas anderes **_nicht tut_** (verworfene Alternativen mit Begründung).

    Da Kommentare schlecht altern (häufig nicht mit Codeänderungen gepflegt werden) sollten v.a. Dinge kommentiert werden, die auch nach Codeanpassungen noch relevant sind. Redundanzen sollten vermieden werden. 

    In Kommentaren kann außerdem stehen, was man im Code selbst nicht mehr sieht: warum habe ich eine Alternative verworfen? Welche Alternativen hatte ich erwogen? Diese Art Kommentar hilft, damit das zukünftige ich (oder andere Entwickler*innen) nicht dasselbe Problem ein zweites Mal auf Irrwegen versuchen zu lösen.

    </span>

1. Welche Möglichkeiten gibt es, offene Arbeitspakete zu dokumentieren? <button onclick="toggleAnswer('ccog14')">Antwort</button>

    <span class="hidden-answer" id="ccog14">

    Beispielsweise:

    - ToDo-Liste
    
    - Issue Tracker
    
    - Taskboard

    </span>

1. Welche Möglichkeiten über das reine Sammeln hinaus ergeben sich durch _Issue-Tracking_? <button onclick="toggleAnswer('ccog15')">Antwort</button>

    <span class="hidden-answer" id="ccog15">

    _Issue-Tracking_ hilft beispielsweise auch dabei, die spontan auftretende Arbeit
    
    - zu priorisieren, 
    
    - zu ordnen, 
    
    - Bearbeiter*innen zuordnen, 
    
    - im Prozess nachzuverfolgen, 
    
    - zu dokumentieren und

    - die Anzahl bzw. den Grad der erledigten Arbeit visualisierbar zu machen (Velocity).

    </span>
 
 1. Warum werden zunächst Integrationstests beschrieben und nicht Unit-Tests? <button onclick="toggleAnswer('ccog16')">Antwort</button>

    <span class="hidden-answer" id="ccog16">

    Integrationstests können geschrieben werden, ohne die Abhängigkeiten der Module untereinander zu lösen. Somit können sie schneller erstellt werden und benötigen in einfachster Form auch weniger Fachwissen des Testframeworks. Das gesamte Konzept _Stubbing_ / _Mocking_ muss beispielsweise für Integrationstests nicht vorhanden sein.

    Außerdem benötigt es häufig weniger Integrationstests, um einen größeren Anteil an Code mit den Tests zu erreichen, da diese einen Stich durch alle Schichten des Programms testen.

    </span>

 1. Warum lohnt es sich, mit hohem Zeitaufwand Tests zu automatisieren? <button onclick="toggleAnswer('ccog17')">Antwort</button>

    <span class="hidden-answer" id="ccog17">

    Durch automatisierte Tests werden die Tests nur einmal erstellt und können immer wieder (ohne großen Zeitaufwand) aufgerufen werden. Durch die Zeitersparnis bei der Testausführung selbst werden die Tests häufiger ausgeführt, auch bei kleineren Änderungen. Dadurch werden Fehler schneller erkannt. Digitale Schulden und teure späte Fehlererkennung werden so vermieden. Die (relativ billige) Zeit, die zu Beginn eines Arbeitspakets in Tests investiert wird verhindert (relativ teure) Zeitaufwendungen gegen Ende des Entwicklungszyklus.

    </span>

 1. Warum sind automatische Tests insbesondere dann wichtig, wenn wir - durch unsere Clean-Code-Vorgaben - häufig und viel Refaktorisieren? <button onclick="toggleAnswer('ccog18')">Antwort</button>

    <span class="hidden-answer" id="ccog18">

    Bei jeder Änderung und bei jedem Refaktorieren kann sich das Team sicher sein, dass die Software noch so funktioniert, wie ursprünglich geplant. Wir verlieren die Angst vor Änderungen. Mit der Sicherheit einer guten Testsuite im Rücken können gefahrloser Refaktorierungen vorgenommen werden: der Code bleibt _easy to change_ (_ETC_).

    </span>

1. Welche Vorteile bieten in Zeiten digitaler Medien Bücher? <button onclick="toggleAnswer('ccog19')">Antwort</button>

    <span class="hidden-answer" id="ccog19">

    Fachbücher behandeln ein Thema in (meist) vorgegebener Reihenfolge, sie sind ablenkungsfrei und standordunabhängig zu lesen (Park, Öffis). In Büchern kann einfach annotiert (Textmarker) und kommentiert (Lesezeichen, PostIt, Seitenrand) werden. Bücher werden häufig noch aufwändiger redigiert, viele Fachbücher behandeln nicht kurzfristige Trends sondern allgemeine Grundsätze, die Zeiten überdauern. Einige Bücher gehen in den Kanon der Standardwerke ein und geben die Sprache vor, die zukünftig vom Fachpublikum verwendet wird.
    
    Bücher sind zum einen als Nachschlagewerke parallel zum Computer wie ein Second-Screen nutzbar. Sie bleiben im physischen Zugriff in der Nähe des Schreibtischs, so lange wir damit arbeiten. Sie geraten nicht so leicht in Vergessenheit wie die Liste der "Muss-ich-noch-lesen"-Lesezeichen.

    </span>

1. Warum schlägt der clean-code-developer eine feste Anzahl an Büchern pro Jahr vor? Warum? Zu viel, zu wenig? <button onclick="toggleAnswer('ccog25')">Antwort</button>

    <span class="hidden-answer" id="ccog25">

    Manchmal kommt es gar nicht auf die aktuelle Zahl an, sondern darauf, dass man sich etwas vornimmt. Die Anzahl an Büchern, die man plant zu lesen, sollte realistisch sein - kleine Paperbacks gehen natürlich schneller als die dicken Standards.

    </span>

1. Welche unterschiedlichen Reviewarten gibt es? Welche kennt und praktiziert ihr? <button onclick="toggleAnswer('ccog20')">Antwort</button>

    <span class="hidden-answer" id="ccog20">

    Es gibt tatsächlich sehr viele unterschiedliche Reviewarten - abhängig davon, wie formalisitisch der Prozess ist, welche Gruppe das Review wie durchführt und um welche Dokumente/Produkte es sich handelt. Ein paar Beispiele sind:

    - Pair Programming (als Prozess)
    
    - Code Review (formell oder informell)
    
    - Walkthrough
    
    - Technisches Review
    
    - Peer Review
    
    - Inspektion

    </span>

1. Welche Fehler können Reviews aufdecken, die durch dynamische Tests nicht (so leicht) aufgedeckt werden können?<button onclick="toggleAnswer('ccog21')">Antwort</button>

    <span class="hidden-answer" id="ccog21">

    zum Beispiel:
    
    - Verstöße gegen Coding Conventions
        
    - missverstandene Anforderungen / logische Fehler
        
    - Compliance Probleme

    </span>

1. Warum werden Reviews als statische Tests bezeichnet? <button onclick="toggleAnswer('ccog22')">Antwort</button>

    <span class="hidden-answer" id="ccog22">

    Bei statischen Testverfahren wird der Code nicht ausgeführt: es wird also getestet, ohne dass ein Compiler/Interpreter genutzt wird. Damit sind Reviews (wie z.B. auch Schreibtischtests) das statische Gegenstück zu den dynamischen Testverfahren (Unittest, Integrationstest, usw.), bei denen der Code ausgeführt wird.

    </span>

1. Welche Dokumente können einem Review unterzogen werden?<button onclick="toggleAnswer('ccog23')">Antwort</button>

    <span class="hidden-answer" id="ccog23">

    Alle Dokumente, die im Verlauf eines Projekts entstehen, können einem Review unterzogen werden: von den Anforderungsdokumenten über die Entwürfe und den Quell- und Testcode bis zu den vorbereiteten Abnahmeprotokollen. Je wichtiger die Dokumente für den Projekterfolg sind, desto eingehender sollte ein Review-Prozess implementiert sein.

    </span>

1. Warum kann sich der doppelte Personaleinsatz beim Pair-Programming lohnen, auch wenn dadurch nicht doppelt so viele Zeilen Code wie bei einem einzelnen Entwickler erzeugt werden? <button onclick="toggleAnswer('ccog24')">Antwort</button>

    <span class="hidden-answer" id="ccog24">

    Hier greift die gleiche Logik wie bei automatisierten Tests und der Testgetriebenen Entwicklung: ein entwickelndes Paar wird einige Fehler bereits in einer sehr frühen Phase entdecken und ausbessern. Dadurch werden teure Folgekosten der späten Fehlerbehebung vermieden.

    </span>


#### Offene Fragen:

1. Welche Tools nutzt ihr für _Issue-Tracking_ in Eurer Firma?

1. Welche Bücher / Medien / Periodika liest Du und kannst Du empfehlen?

1. In welchen Bereichen planst Du, dich fortzubilden?


#### Allgemeine Begriffe: Was meint clean-code-developer mit...:


Folgende Begriffe werden im Orangenen Grad genannt. Wenn Du sie nicht kennst, solltest Du mal nachschlagen, was sie bedeuten:

- Kohäsion    

- lose Kopplung

- Aspektorientierte Programmierung     

- Domain Driven Design

### Links und weitere Informationen

- [Oranger Grad des Clean-Code-Developers](https://clean-code-developer.de/die-grade/oranger-grad/)
